import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Student } from '../models/student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private getUrl: string = "http://localhost:8080/api/v1/students";

  constructor(private httpClient: HttpClient) { }

  getStudents(): Observable<Student[]> {
    return this.httpClient.get<Student[]>(this.getUrl).pipe(
      map(res => res)
    )
  }

  // save new student
  saveStudent(student: Student): Observable<Student> {
    return this.httpClient.post<Student>(this.getUrl, student);
  }

  // update student
  getStudent(id: number): Observable<Student> {
    return this.httpClient.get<Student>(`${this.getUrl}/${id}`).pipe(
      map(res => res)
    )
  }

  // delete student
  deleteStudent(id: number): Observable<any> {
    return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: "text"});
  }

}
