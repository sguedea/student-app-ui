export interface Student {
    student_id: number;
    student_name: string;
    student_email: string;
    student_branch: string;

}
