import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/models/student';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  students: Student[] = [];

  constructor(private studentService: StudentService) { }

  ngOnInit(): void {
    this.listStudents();
  }

  listStudents() {
    this.studentService.getStudents().subscribe(
      data => this.students = data
    )
  }

  // delete student
  deleteStudent(id: number) {
    this.studentService.deleteStudent(id).subscribe(
      data => {
        console.log('delete response', data);
        this.listStudents();
      }
    )
  }

}
