import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Student } from 'src/app/models/student';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {
  // properties
  student: Student = {
    student_id: 0,
    student_name: "",
    student_email: "",
    student_branch: ""
  }

  cohort = [
    {id: 1, name: "Alpha"},
    {id: 2, name: "Bravo"},
    {id: 3, name: "Charlie"},
    {id: 4, name: "Delta"}
  ];
 selectedValue = null;

  // dependency injection
  constructor(private studentService: StudentService,
              private router: Router,
              private activeRoute: ActivatedRoute) 
              { }

  ngOnInit(): void {
    // update student
    var isIdPresent = this.activeRoute.snapshot.paramMap.has('id');
    if (isIdPresent) {
      var studentId = this.activeRoute.snapshot.paramMap.get('id');
      this.studentService.getStudent(Number(studentId)).subscribe(
        data => this.student = data
      )
    }
  }

  // method to save
  saveStudent() {
    this.studentService.saveStudent(this.student).subscribe(
      data => {
        this.router.navigateByUrl("/view-students");
      }
    )
  }

  // delete student
  deleteStudent(id: number) {
    this.studentService.deleteStudent(id).subscribe(
      data => {
        console.log('deleted response', data);
        this.router.navigateByUrl('/view-students');
      }
    )
  }

}
